package com.websystique.springboot.controller;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.webservicex.GlobalWeather;
import com.webservicex.GlobalWeatherSoap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
 

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.websystique.springboot.model.Country;
import com.websystique.springboot.model.User;
import com.websystique.springboot.service.CountryService;
import com.websystique.springboot.util.CustomErrorType;
 
@RestController
@RequestMapping("/api")
public class RestApiController {
	 GlobalWeather weather = new GlobalWeather();
	 GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
 
    public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);
 
    @Autowired
    CountryService countryService;
    
 
    @RequestMapping(value = "/user", method = RequestMethod.GET)
   // public ResponseEntity<List<User>> listAllUsers() {
     public ResponseEntity<List<String>> listAllUsers() {
    	 GlobalWeather weather = new GlobalWeather();
         GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
         String cities = soap.getCitiesByCountry("India");
         System.out.println("------------------------------------------------");
         System.out.println("------------------------------------------------"+cities);
         
         List<String> cityList = countryService.findAllCountry("India");
    	

        return new ResponseEntity<List<String>>(cityList, HttpStatus.OK);
    }
 
    
    @RequestMapping(value = "/cntry/{country}", method = RequestMethod.GET)
    public ResponseEntity<?> getCity(@PathVariable("country") String country) {
        logger.info("Fetching city with name {}", country);
        List<String> cityList = countryService.findById(country);
       
        if (cityList.isEmpty()) {
            logger.error("Country with city {} not found.", country);
            return new ResponseEntity(new CustomErrorType("User with id " + country 
                    + " not found"), HttpStatus.NOT_FOUND);
        }
       
        return new ResponseEntity<List<String>>(cityList, HttpStatus.OK);
    }
    
    
   
    @RequestMapping(value = "/country/", method = RequestMethod.POST)
    public ResponseEntity<?> createCity(@RequestBody Country country, UriComponentsBuilder ucBuilder) {
    	System.out.println("Creating User : {}");
    	List<Country> cityList = countryService.createCity(country.getCountry(), country.getCity());
    	return new ResponseEntity<List<Country>>(cityList, HttpStatus.OK);
    }
    
    
   
    
    
    
    
    @RequestMapping(value = "/country/{id}", method = RequestMethod.PUT)
    
    public ResponseEntity<?> updateCity(@RequestBody Country country,@PathVariable("id") String cityName) {
        logger.info("Updating User with city {}", country.getCity());
        List<Country> cityList = countryService.updateCityName(country.getCountry(),country.getCity(),cityName);
        return new ResponseEntity<List<Country>>(cityList, HttpStatus.OK);
    }
    
    
    
@RequestMapping(value = "/country/", method = RequestMethod.DELETE)
    
    public ResponseEntity<?> deleteCity(@RequestBody Country country) {
        logger.info("deleting User with city {}", country.getCity());
        List<Country> cityList = countryService.deleteCity(country.getCountry(),country.getCity());
        return new ResponseEntity<List<Country>>(cityList, HttpStatus.OK);
    }
 
  
 
}
