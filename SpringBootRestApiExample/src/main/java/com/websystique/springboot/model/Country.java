package com.websystique.springboot.model;

import org.hibernate.validator.internal.util.IgnoreJava6Requirement;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Country {
	@JsonProperty("city")
	String city;
	@JsonProperty("country")
	String country;
	
	//String updatedCityName;
	
	

 /* public String getUpdatedCityName() {
		return updatedCityName;
	}

	public void setUpdatedCityName(String updatedCityName) {
		this.updatedCityName = updatedCityName;
	}*/

public Country() {
		super();
	}

public Country(String country, String city){
	  this.country = country;
	  this.city = city;
	  
  }

public String getCountry() {
	return country;
}

public void setCountry(String country) {
	this.country = country;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}
  

@Override
public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + 32;
    return result;
}

@Override
public boolean equals(Object obj) {
    if (this == obj)
        return true;
    if (obj == null)
        return false;
    if (getClass() != obj.getClass())
        return false;
    Country other = (Country) obj;
    
    return true;
}

@Override
public String toString() {
    return "Country [city=" + city + ", state=" + country  + "]";
}
  

}
