package com.websystique.springboot.service;

import java.util.List;

import com.websystique.springboot.model.Country;
import com.websystique.springboot.model.User;

public interface CountryService {
	 List<String> findAllCountry(String CountryName);
	 List<String> findById(String city);
	 List<Country> createCity(String countryName,String city);
	 List<Country> updateCityName(String countryName,String city,String updatedCityName);
	 List<Country> deleteCity(String countryName,String city);

}
    