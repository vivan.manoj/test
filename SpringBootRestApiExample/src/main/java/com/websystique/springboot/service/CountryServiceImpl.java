package com.websystique.springboot.service;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.webservicex.GlobalWeather;
import com.webservicex.GlobalWeatherSoap;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.websystique.springboot.model.Country;
import com.websystique.springboot.model.User;

@Service("countryService")
public class CountryServiceImpl implements CountryService {
	private  List<String> countryList;
	private  List<String> cityList;
	
	 private static List<Country> users;
     
	    /*static{
	        users= populateDummyUsers(String countryName);
	    }*/
	  
	  public List<String> findAllCountry(String countryName) {
		  System.out.println("inside CountryServiceImpl");
		 countryList = new ArrayList<String>();
		 GlobalWeather weather = new GlobalWeather();
	     GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
	     String cities = soap.getCitiesByCountry(countryName);

	   try {
	         DocumentBuilder documentBuilder = DocumentBuilderFactory
	                                     .newInstance().newDocumentBuilder();
	         InputSource is = new InputSource();
	         is.setCharacterStream(new StringReader(cities));
	         Document doc = documentBuilder.parse(is);
	         NodeList nodes = doc.getElementsByTagName("Table");
	         for (int i = 0; i < nodes.getLength(); i++) {
	                 Element element = (Element) nodes.item(i);
	                 NodeList subnode = element.getElementsByTagName("City");
	                 Element subelement = (Element) subnode.item(0);
	                 countryList.add(subelement.getTextContent());
	                 //System.out.println(subelement.getTextContent());
	                 
	           }
	         System.out.println("++++++++++++"+countryList.size());
	        } catch (ParserConfigurationException e) {
	                e.printStackTrace();
	        } catch (SAXException e) {
	                e.printStackTrace();
	        } catch (IOException e) {
	                e.printStackTrace();
	     }
		return countryList;
	}

	  
	  public List<String> findById(String countryName) {
		  System.out.println("inside CountryServiceImpl..");
		  cityList = new ArrayList<String>();
		 GlobalWeather weather = new GlobalWeather();
	     GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
	     String cities = soap.getCitiesByCountry(countryName);

	   try {
	         DocumentBuilder documentBuilder = DocumentBuilderFactory
	                                     .newInstance().newDocumentBuilder();
	         InputSource is = new InputSource();
	         is.setCharacterStream(new StringReader(cities));
	         Document doc = documentBuilder.parse(is);
	         NodeList nodes = doc.getElementsByTagName("Table");
	         for (int i = 0; i < nodes.getLength(); i++) {
	                 Element element = (Element) nodes.item(i);
	                 NodeList subnode = element.getElementsByTagName("City");
	                 Element subelement = (Element) subnode.item(0);
	                 cityList.add(subelement.getTextContent());
	                 //System.out.println(subelement.getTextContent());
	                 
	           }
	         System.out.println("++++++++++++"+cityList.size());
	        } catch (ParserConfigurationException e) {
	                e.printStackTrace();
	        } catch (SAXException e) {
	                e.printStackTrace();
	        } catch (IOException e) {
	                e.printStackTrace();
	     }
		return cityList;
	}
	  
	  
	  public void saveUser(String cityName) {
	       
	    }
	  
	  public  List<Country> createCity(String countryName,String CityName){
	        List<Country> cityList = new ArrayList<Country>();
	        	cityList = new ArrayList<Country>();
			 GlobalWeather weather = new GlobalWeather();
		     GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
		     String cities = soap.getCitiesByCountry(countryName);

		   try {
		         DocumentBuilder documentBuilder = DocumentBuilderFactory
		                                     .newInstance().newDocumentBuilder();
		         InputSource is = new InputSource();
		         is.setCharacterStream(new StringReader(cities));
		         Document doc = documentBuilder.parse(is);
		         NodeList nodes = doc.getElementsByTagName("Table");
		         for (int i = 0; i < nodes.getLength(); i++) {
		                 Element element = (Element) nodes.item(i);
		                 NodeList subnode = element.getElementsByTagName("City");
		                 Element subelement = (Element) subnode.item(0);
		                 cityList.add(new Country(countryName, subelement.getTextContent()));
		                 System.out.println("++++++old++++++"+cityList.size());
		                
		                 //System.out.println(subelement.getTextContent());
		                 
		           }
		         if(cityList.size()>0){
                	 cityList.add(new Country(countryName, CityName));
                 }
		         System.out.println("++++++++++++"+cityList.size());
		        } catch (ParserConfigurationException e) {
		                e.printStackTrace();
		        } catch (SAXException e) {
		                e.printStackTrace();
		        } catch (IOException e) {
		                e.printStackTrace();
		     }
			
	        
	        return cityList;
	    }
	
	  
	  public  List<Country> updateCityName(String countryName,String updatedCityName,String CityName){
	        List<Country> cityList = new ArrayList<Country>();
	        System.out.println("inside CountryServiceImpl..");
	        cityList = new ArrayList<Country>();
			 GlobalWeather weather = new GlobalWeather(); 
		     GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
		     String cities = soap.getCitiesByCountry(countryName);

		   try {
		         DocumentBuilder documentBuilder = DocumentBuilderFactory
		                                     .newInstance().newDocumentBuilder();
		         InputSource is = new InputSource();
		         is.setCharacterStream(new StringReader(cities));
		         Document doc = documentBuilder.parse(is);
		         NodeList nodes = doc.getElementsByTagName("Table");
		         for (int i = 0; i < nodes.getLength(); i++) {
		                 Element element = (Element) nodes.item(i);
		                 NodeList subnode = element.getElementsByTagName("City");
		                 Element subelement = (Element) subnode.item(0);
		                 cityList.add(new Country(countryName, subelement.getTextContent()));
		                
		                
		                 //System.out.println(subelement.getTextContent());
		                 
		           }
		       
		         Country val = new Country();
		         val.setCountry(countryName);
		         val.setCity(CityName);
		         int k =  0;
		       
		         for(int j=0 ; j<cityList.size();j++){
		        	 if((cityList.get(j).getCity()).equalsIgnoreCase(CityName)){
		        		 k = j;
		        	 }
		         }
		         
		       
		        
		         val.setCity(updatedCityName);
		         cityList.set(k, val);
		       
		         System.out.println("++++++new++++++"+cityList.get(4));
		         
		        
		         System.out.println("++++++++++++"+cityList.size());
		        } catch (ParserConfigurationException e) {
		                e.printStackTrace();
		        } catch (SAXException e) {
		                e.printStackTrace();
		        } catch (IOException e) {
		                e.printStackTrace();
		     }
			
	        
	        return cityList;
	    }
	
	  
	  public  List<Country>deleteCity(String countryName,String CityName){
	        List<Country> cityList = new ArrayList<Country>();
	        System.out.println("inside CountryServiceImpl..");
	        System.out.println("CityName..::"+CityName);
	        System.out.println("countryName::"+countryName);
			 cityList = new ArrayList<Country>();
			 GlobalWeather weather = new GlobalWeather(); 
		     GlobalWeatherSoap soap = weather.getGlobalWeatherSoap();
		     String cities = soap.getCitiesByCountry(countryName);

		   try {
		         DocumentBuilder documentBuilder = DocumentBuilderFactory
		                                     .newInstance().newDocumentBuilder();
		         InputSource is = new InputSource();
		         is.setCharacterStream(new StringReader(cities));
		         Document doc = documentBuilder.parse(is);
		         NodeList nodes = doc.getElementsByTagName("Table");
		         for (int i = 0; i < nodes.getLength(); i++) {
		                 Element element = (Element) nodes.item(i);
		                 NodeList subnode = element.getElementsByTagName("City");
		                 Element subelement = (Element) subnode.item(0);
		                 cityList.add(new Country(countryName, subelement.getTextContent()));
		                 System.out.println("++++++old++++++"+cityList.size());
		                
		                 //System.out.println(subelement.getTextContent());
		                 
		           }
		       
		        int k = 0;
		       
		         for(int j=0 ; j<cityList.size();j++){
		        	 if((cityList.get(j).getCity()).equalsIgnoreCase(CityName)){
		        		 k = j;
		        	 }
		         }
		         
		         System.out.println("k"+ k);
		        
		        
		         cityList.remove(k);
		     
		         System.out.println("++++++new++++++"+cityList.get(4));
		         
		         
		         
		         
		        
		         System.out.println("++++++++++++"+cityList.size());
		        } catch (ParserConfigurationException e) {
		                e.printStackTrace();
		        } catch (SAXException e) {
		                e.printStackTrace();
		        } catch (IOException e) {
		                e.printStackTrace();
		     }
			
	        
	        return cityList;
	    }
	
	
}
